FROM blackcowtech/atlassian-base
LABEL maintainer="Paul Jefferson <paul.jefferson@blackcow-technology.co.uk>"

ARG BITBUCKET_VERSION

ENV \
  BITBUCKET_HOME=${APP_HOME}/bitbucket \
  BITBUCKET_INSTALL=${APP_INSTALL}/bitbucket

EXPOSE 7990
EXPOSE 7999

# from https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz
COPY atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz /tmp/

RUN set -ex \
  && apk add --update --no-cache --virtual .build-deps \
  tar \
  && apk add --no-cache --virtual .run-deps \
  procps \
  shadow \
  && apk add --no-cache --virtual .bitbucket-deps \
  git \
  openssh \
  perl \
  && mkdir -p ${BITBUCKET_HOME} \
  && mkdir -p ${BITBUCKET_INSTALL} \
  && usermod -d ${BITBUCKET_HOME} ${APP_USER} \
  && tar xzf /tmp/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz -C ${BITBUCKET_INSTALL} --strip-components=1 \
  && chown -R ${APP_USER}:${APP_GROUP} ${BITBUCKET_HOME} \
  && chown -R ${APP_USER}:${APP_GROUP} ${BITBUCKET_INSTALL} \
  # tidy up
  && apk del .build-deps \
  && rm -rf /tmp/* \
  && rm -rf /var/log/*

WORKDIR ${BITBUCKET_HOME}
VOLUME ["${BITBUCKET_HOME}"]

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]
CMD ["bitbucket"]
