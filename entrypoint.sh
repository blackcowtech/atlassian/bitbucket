#!/usr/bin/env bash

set -eo pipefail

if [ "$1" != "bitbucket" ]
then
    exec su-exec ${APP_USER} "$@"
fi

# remove dummy bitbucket argument
ARGS="${@:2}"

function updateProperty() {
    local filepath=$1; local key=$2; local val=$3

    if grep -q "${key}=" ${filepath}
    then
        sed -i "s#\(${key}=\).*#\1${val}#" ${filepath}
    else
        echo "${key}=${val}" >>${filepath}
    fi
}

BITBUCKET_PROPERTIES=${BITBUCKET_HOME}/shared/bitbucket.properties
mkdir -p $(dirname ${BITBUCKET_PROPERTIES}) && touch ${BITBUCKET_PROPERTIES}

sed -i '/server.proxy-name/d' ${BITBUCKET_PROPERTIES}
sed -i '/server.proxy-port/d' ${BITBUCKET_PROPERTIES}
sed -i '/server.scheme/d' ${BITBUCKET_PROPERTIES}
sed -i '/server.secure/d' ${BITBUCKET_PROPERTIES}

if [ -n "${X_PROXY_NAME}" ]
then
    updateProperty ${BITBUCKET_PROPERTIES} "server.proxy-name" ${X_PROXY_NAME}
fi

if [ -n "${X_PROXY_PORT}" ]
then
    updateProperty ${BITBUCKET_PROPERTIES} "server.proxy-port" ${X_PROXY_PORT}
fi

if [ -n "${X_PROXY_SCHEME}" ]
then
    updateProperty ${BITBUCKET_PROPERTIES} "server.scheme" ${X_PROXY_SCHEME}

    if [ "${X_PROXY_SCHEME}" = "https" ]
    then
        updateProperty ${BITBUCKET_PROPERTIES} "server.secure" "true"
    else
        updateProperty ${BITBUCKET_PROPERTIES} "server.secure" "false"
    fi
fi

umask 0027
chown -R ${APP_USER}:${APP_GROUP} ${BITBUCKET_HOME}
exec su-exec ${APP_USER} ${BITBUCKET_INSTALL}/bin/start-bitbucket.sh -fg ${ARGS}
